"""
This is a sample library function
"""

import os
import subprocess
import shutil


def check_root():
    """ Check if current user has root privileges """
    if os.geteuid() == 0:
        return True
    else:
        return False


def get_ip_address():
    hostname_out = subprocess.check_output(['hostname', '-I'])
    ip_address_str = hostname_out.decode('ascii').split()[0]
    return ip_address_str


def create_directory(path, permission):
    try:
        os.makedirs(path, mode=permission, exist_ok=True)
    except OSError:
        return False
    return True


def write_to_file(content, path_to_file, mode):
    """
    Write content to a file
    Args:
        content: what to write inside file
        path_to_file: path to file
        mode: write (w+), append (a), etc..
    """
    file = open(path_to_file, mode)
    file.write("\n")
    file.write(content)
    file.close()


def check_if_a_command_installed(command_name):
    """
    Check if a command is installed on a VM
    Args:
        command_name(str): Name of the command
    Returns
        bool: True if present, False otherwise
    """
    return shutil.which(command_name) is not None


def delete_file(file_path):
    """
    deletes the file at specified path

    Args
        file_path(str): path at which the file is present
    """
    if os.path.exists(file_path):
        os.remove(file_path)


def copy_file(source_file, destination_file):
    """
    Copy file from source file to destination file
    Args:
        source_file(str): Absolute path of the file
        destination_file(str): Absolute destination path of the file
    """
    if not os.path.exists(source_file):
        # Fail silently
        return
    if os.path.exists(destination_file):
        delete_file(destination_file)
    shutil.copy2(source_file, destination_file)

