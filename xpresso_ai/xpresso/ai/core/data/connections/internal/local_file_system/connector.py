""" LocalFSConnector class for connectivity to local filesystem """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import os
from io import BytesIO
import pandas as pd
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.abstract_fs import AbstractFSConnector
from xpresso.ai.core.logging.xpr_log import XprLogger


class LocalFSConnector(AbstractFSConnector):
    """

    DataConnector class for connecting to user's local filesystem.

    """

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API.

        """

        self.client = None

    def getlogger(self):
        """

        Xpresso logger built on top of python module.

        Returns:
            object: xpresso logger object.

        """

        return XprLogger()

    def connect(self, config):
        """

        Client-API is not involved for connecting to LocalFS.
        Hence, this method does not support LocalFSConnector class.

        """

        return None

    def import_files(self, config):
        """

        Importing directory/files for unstructured data analysis.

        :Args:
            config (dict): A JSON object, input by the user, that states
                the file to be imported.

        Returns:
            object: a pandas DataFrame.

        """

        file_extension = os.path.splitext(config.get(constants.input_path))[1]
        if file_extension == constants.text_extension or \
                file_extension == constants.csv_extension or \
                file_extension == constants.excel_extension:
            data = pd.DataFrame([[os.path.basename(config.get
                                                   (constants.input_path)),
                                  str(os.stat(config.get
                                              (constants.input_path)).st_size /
                                      constants.CONV_FACTOR),
                                  config.get
                                  (constants.input_path)]],
                                columns=[constants.FILE_NAME_COL,
                                         constants.FILE_SIZE_COl,
                                         constants.FILE_PATH_COL])
            data[constants.FILE_SIZE_COl] = \
                data[constants.FILE_SIZE_COl].astype(float)
        else:
            data = {
                constants.FILE_NAME_COL: [],
                constants.FILE_SIZE_COl: [],
                constants.FILE_PATH_COL: []
            }
            with os.scandir(config.get(constants.input_path)) as it:
                for entry in it:
                    data[constants.FILE_NAME_COL].append(entry.name)
                    data[constants.FILE_SIZE_COl].append(
                        str(os.stat(config.get
                                    (constants.input_path)).st_size /
                            constants.CONV_FACTOR))
                    data[constants.FILE_PATH_COL]\
                        .append(os.path.join(config.get(constants.input_path),
                                             entry.name))
            data = pd.DataFrame(data)
            data[constants.FILE_SIZE_COl] = \
                data[constants.FILE_SIZE_COl].astype(float)
        return data

    def import_dataframe(self, config, **kwargs):
        """

        Importing data from a user specified file in local filesystem.

        Args:
            config (dict): A JSON object, input by the user, that states
                the file to be imported.

        Returns:
            object: a pandas DataFrame.

        """

        data_frame = None
        file_extension = os.path.splitext(config.get(constants.input_path))[1]
        try:
            if file_extension == constants.excel_extension:
                with open(config.get(constants.input_path), 'rb') as file:
                    byte_format = BytesIO(file.read())
                data_frame = pd.read_excel(byte_format, **kwargs)
            elif file_extension == constants.csv_extension:
                data_frame = pd.read_csv(config.get(constants.input_path),
                                         **kwargs)
            elif file_extension == constants.text_extension:
                data_frame = pd.read_csv(config.get(constants.input_path),
                                         **kwargs)
            else:
                raise xpr_exp.FSClientUnsupportedFileType
        except FileNotFoundError:
            self.getlogger().error("Invalid File name. File not found.")
        return data_frame

    def close(self):
        """

        LocalFSConnector class does not support any API.
        Hence, this method is not required.

        """

        return None
